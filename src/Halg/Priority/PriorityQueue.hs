{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FunctionalDependencies #-}

module Halg.Priority.PriorityQueue (
	PriorityQueue, findMin, merge, insert, deleteMin
) where

import Data.Maybe (Maybe)

class (Ord k) => PriorityQueue p k v | p -> k v where
	-- basic operations
	findMin		:: p -> Maybe v
	merge		:: p -> p -> p
	insert		:: p -> (k, v) -> p
	deleteMin	:: p -> (Maybe v, p)
