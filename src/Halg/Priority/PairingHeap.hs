{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Halg.Priority.PairingHeap (PairingHeap, decreaseKey) where

import Halg.Priority.PriorityQueue
import Data.Maybe (Maybe)

test :: PairingHeap Int String
test = Heap (0,"foop") [Heap (5,"moop") [],Heap (1,"hello") [Heap (2,"blarg") []]]

data PairingHeap a b =
	Empty
	| Heap (a, b) [PairingHeap a b]
	deriving (Show)

instance (Ord a) => PriorityQueue (PairingHeap a b) a b where
	findMin Empty				= Nothing
	findMin (Heap (_, b) _) 	= Just b

	merge Empty h = h
	merge h Empty = h
	merge h1@(Heap node1@(a1, _) c1) h2@(Heap node2@(a2, _) c2)
		| a1 < a2	= Heap node1 (h2:c1)
		| otherwise	= Heap node2 (h1:c2)

	insert h node = merge h (Heap node [])

	deleteMin Empty				= (Nothing, Empty)
	deleteMin (Heap (_, b) cs)	= (Just b, mergePairs cs)

mergePairs :: (Ord a) => [PairingHeap a b] -> PairingHeap a b
mergePairs []			= Empty
mergePairs (h:[])		= h
mergePairs (h1:h2:hs)	= merge (merge h1 h2) (mergePairs hs)

decreaseKey	:: (Ord a) => PairingHeap a b -> (b -> Bool) -> a -> PairingHeap a b
	-- second argument is a predicate over values, to determine which node to decrease
decreaseKey Empty _ _		= Empty
decreaseKey h pred newKey	=
	let
		(st, h') = removeMatchingSubtree h pred
		st' = replaceKey st' newKey
	in merge h' st'

removeMatchingSubtree :: PairingHeap a b -> (b -> Bool) -> (PairingHeap a b, PairingHeap a b)
removeMatchingSubtree Empty _ = (Empty, Empty)
removeMatchingSubtree h@(Heap n@(_, b) cs) pred
	| pred b || null cs	= (Empty, h)
	| otherwise	=
		let (st, cs') = foldr (remSubtreeFolder pred) (Empty, []) cs
		in (st, Heap n cs')

type SubtreeAcc a b = (PairingHeap a b, [PairingHeap a b])

remSubtreeFolder :: (b -> Bool) -> PairingHeap a b -> SubtreeAcc a b -> SubtreeAcc a b
remSubtreeFolder _ c (h@(Heap _ _), cs) = (h, c:cs)	-- If we already found a match, don't even check
remSubtreeFolder _ Empty (h, cs) = (h, cs)
remSubtreeFolder pred c@(Heap (_, v) _) (Empty, cs)
	| pred v	= (c, cs)
	| otherwise	=
		case removeMatchingSubtree c pred of	-- match recursively in children
			(Empty, _)	-> (Empty, c:cs)		-- wasn't in children either
			(cc, c')	-> (cc, c':cs)			-- found match in children, pass it up and tack on new child

replaceKey :: PairingHeap a b -> a -> PairingHeap a b
replaceKey Empty _				= Empty
replaceKey (Heap (_, b) cs) k'	= Heap (k', b) cs
